# Material Database an web UI 

Python based material database with a flask backend: https://flask.palletsprojects.com/en/3.0.x/
Backend adds materials to a MongoDB based database: https://www.mongodb.com/try/download/community

MongoDB is required to be running for the program to work: https://www.mongodb.com/docs/manual/administration/install-community/

Hosts a local webpage for its interface. 

Uses javascript for web interactivity mostly for dynamic the dynamic forms and graphs. 

Basic use: 

Open the web page, add material properties either as constants or as a 2d matrix. If a material property is not in the database, a new one can be added. This property will automatically be added to the database when the material is submitted. Also give the material a name.

Once the material has been submitted the administration will have to verify and accept it. Once it has been accepted it will be added to the finished materials folder in the mongodb database.

## step by step: 


1. start the MongoDB database, use command: "C:\Program Files\MongoDB\Server\7.0\bin\mongod.exe" --dbpath="c:\data\db"
2. Make sure the folders data and db exists like in the path above. 
3. Open the app.py file, make sure all dependencies are installed. 
4. Run the app.py file. 
5. it should give you an IP in the python console that should link to the hosted website.
6. You can check the status of the databse using MongoDB compass that is usualy bundled with the MongoDB install. 
